package gwendal.utils;

import javax.persistence.EntityManager;

/**
 * Creates an EntityManager from the AppListener's EntityManagerFactory
 */
public final class JPAUtil {
	private JPAUtil() {}
	
	public static EntityManager getEntityManager() {
		return AppListener.getEmf().createEntityManager();
	}
}
