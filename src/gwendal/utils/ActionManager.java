package gwendal.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import gwendal.business.CreateFidelityAction;
import gwendal.business.CreatePersonAction;
import gwendal.business.DeleteFidelityAction;
import gwendal.business.DeletePersonAction;
import gwendal.business.FidelitiesAction;
import gwendal.business.FidelityAction;
import gwendal.business.HomeAction;
import gwendal.business.PersonAction;
import gwendal.business.PersonsAction;
import gwendal.business.UpdateFidelityAction;
import gwendal.business.UpdatePersonAction;

/**
 * A hashmap of all the actions from the application, returns the correct one to the servlet depending on the URI
 */
public final class ActionManager {
	private static Map<String, AbstractAction> actions = new HashMap<>();
	private ActionManager() {}
	
	static {
		actions.put("home", new HomeAction());
		actions.put("persons", new PersonsAction());
		actions.put("fidelities", new FidelitiesAction());
		actions.put("person", new PersonAction());
		actions.put("fidelity", new FidelityAction());
		actions.put("fidelity", new FidelityAction());
		actions.put("updateperson", new UpdatePersonAction());
		actions.put("createperson", new CreatePersonAction());
		actions.put("updatefidelity", new UpdateFidelityAction());
		actions.put("createfidelity", new CreateFidelityAction());
		actions.put("deleteperson", new DeletePersonAction());
		actions.put("deletefidelity", new DeleteFidelityAction());
	}
	
	public static AbstractAction getAction(HttpServletRequest request) {
		String actionName = getActionName(request);
		return actions.get(actionName);
	}
	
	public static String getActionName(HttpServletRequest request) {
		String uri = request.getRequestURI();
		return uri.substring(uri.lastIndexOf("/")+1);
	}
}
