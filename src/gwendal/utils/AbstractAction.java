package gwendal.utils;

import javax.servlet.http.HttpServletRequest;

public abstract class AbstractAction {
	public abstract String executeAction(HttpServletRequest request);
}
