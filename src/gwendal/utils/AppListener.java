package gwendal.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppListener implements ServletContextListener {
	private static final String PU = "test_sequoiasoft";
	
	private static EntityManagerFactory emf;
	
	public static EntityManagerFactory getEmf() { 
		return emf; 
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// Closes the EMF at the end of the application
		if (emf != null && emf.isOpen()) {
			emf.close();
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// Creates an EntityManagerFactory from persistence.xml at the start of the application
		emf = Persistence.createEntityManagerFactory(PU);
	}
}
