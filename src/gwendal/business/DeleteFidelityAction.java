package gwendal.business;

import javax.servlet.http.HttpServletRequest;

import gwendal.model.dao.DAOFactory;
import gwendal.utils.AbstractAction;

public class DeleteFidelityAction extends AbstractAction{

	@Override
	public String executeAction(HttpServletRequest request) {		
		String idStr = request.getParameter("id");
		if (idStr != null) {
			try {
				// I parse the id I get from GET into an int and use it to remove the object from the database
				int id = Integer.parseInt(idStr);
				DAOFactory.getFidelityDAO().delete(id);
			}
			catch (Exception e) { e.printStackTrace(); }
		}
		
		return "HomeJSP";
	}

}
