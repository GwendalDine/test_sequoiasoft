package gwendal.business;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import gwendal.model.beans.Fidelity;
import gwendal.model.dao.DAOFactory;
import gwendal.utils.AbstractAction;

public class CreateFidelityAction extends AbstractAction{
	
	@Override
	public String executeAction(HttpServletRequest request) {
		
		// I need to format two strings from the form into a date
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		if (request.getMethod().equals("POST")) {
			// I retrieve the form values
			String idPersonSTR = request.getParameter("idPerson");
			String code = request.getParameter("code");
			String beginStr = request.getParameter("begin");
			String endStr = request.getParameter("end");
			String bonusStr = request.getParameter("bonus");
			
			try {
				// I parse the needed values
				int idPerson = Integer.parseInt(idPersonSTR);
				int bonus = Integer.parseInt(bonusStr);
				Date begin = formatter.parse(beginStr);
				Date end = formatter.parse(endStr);
				// I create a new instance of Fidelity and persist it in the database
				Fidelity fidelity = new Fidelity(idPerson, code, begin, end, bonus);
				DAOFactory.getFidelityDAO().create(fidelity);
				request.setAttribute("redirectUrl", "/fidelity?id=" + fidelity.getIdPerson());
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return "FidelityFormJSP";
	}

}