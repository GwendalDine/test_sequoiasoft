package gwendal.business;

import javax.servlet.http.HttpServletRequest;

import gwendal.utils.AbstractAction;

public class HomeAction extends AbstractAction{

	@Override
	public String executeAction(HttpServletRequest request) {
		
		return "HomeJSP";
	}

}
