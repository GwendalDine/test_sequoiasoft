package gwendal.business;

import javax.servlet.http.HttpServletRequest;

import gwendal.model.beans.Fidelity;
import gwendal.model.dao.DAOFactory;
import gwendal.utils.AbstractAction;

public class FidelityAction extends AbstractAction {

	@Override
	public String executeAction(HttpServletRequest request) {
		String idStr = request.getParameter("id");
		if (idStr != null) {
			try {
				// I parse the id I get from GET into an int and use it to find a fidelity from the database
				int id = Integer.parseInt(idStr);
				Fidelity fidelity = DAOFactory.getFidelityDAO().find(id);
				request.setAttribute("fidelity", fidelity);
			}
			catch (Exception e) { e.printStackTrace(); }
		}
		
		return "FidelityJSP";
	}

}
