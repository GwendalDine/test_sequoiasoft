package gwendal.business;

import javax.servlet.http.HttpServletRequest;

import gwendal.model.beans.Person;
import gwendal.model.dao.DAOFactory;
import gwendal.utils.AbstractAction;

public class PersonAction extends AbstractAction {

	@Override
	public String executeAction(HttpServletRequest request) {		
		String idStr = request.getParameter("id");
		if (idStr != null) {
			try {
				// I parse the id I get from GET into an int and use it to find a person from the database
				int id = Integer.parseInt(idStr);
				Person person = DAOFactory.getPersonDAO().find(id);
				request.setAttribute("person", person);
			}
			catch (Exception e) { e.printStackTrace(); }
		}
		
		return "PersonJSP";
	}

}
