package gwendal.business;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import gwendal.model.beans.Person;
import gwendal.model.dao.DAOFactory;
import gwendal.utils.AbstractAction;

public class PersonsAction extends AbstractAction {

	@Override
	public String executeAction(HttpServletRequest request) {
		// I retrieve a list of every persons from the dabatase
		List<Person> persons = DAOFactory.getPersonDAO().findAll();
		
		request.setAttribute("persons", persons);
		
		return "PersonsJSP";
	}

}
