package gwendal.business;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import gwendal.model.beans.Fidelity;
import gwendal.model.dao.DAOFactory;
import gwendal.utils.AbstractAction;

public class UpdateFidelityAction extends AbstractAction {

	@Override
	public String executeAction(HttpServletRequest request) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		boolean postMethod = request.getMethod().equals("POST");
		
		Fidelity fidelity = null;
		String idStr = request.getParameter("id");
		
		try{
			try{
				// I parse the ID from GET and find the corresponding fidelity
				int id = Integer.parseInt(idStr);
				fidelity = DAOFactory.getFidelityDAO().find(id);
			}catch(Exception e){ 
				e.printStackTrace();
			}

			// If I haven't submitted the form yet, I pass the fidelity object to the view to fill the form
			if(!postMethod){
				request.setAttribute("fidelity", fidelity);
			}else{
				// I retrieve the data from the form
				String idPersonSTR = request.getParameter("idPerson");
				String code = request.getParameter("code");
				String beginStr = request.getParameter("begin");
				String endStr = request.getParameter("end");
				String bonusStr = request.getParameter("bonus");
				
				// I parse the needed data into the correct types
				int idPerson = Integer.parseInt(idPersonSTR);
				int bonus = Integer.parseInt(bonusStr);
				Date begin = formatter.parse(beginStr);
				Date end = formatter.parse(endStr);

				// I hydrate the person object
				fidelity.setIdPerson(idPerson);
				fidelity.setCode(code);
				fidelity.setBegin(begin);
				fidelity.setEnd(end);
				fidelity.setBonus(bonus);

				// I update it in the database
				DAOFactory.getFidelityDAO().update(fidelity);
				
				request.setAttribute("redirectUrl", "/fidelity?id=" + fidelity.getIdPerson());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return "FidelityFormJSP";
	}
}