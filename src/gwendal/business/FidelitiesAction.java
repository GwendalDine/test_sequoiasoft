package gwendal.business;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import gwendal.model.beans.Fidelity;
import gwendal.model.dao.DAOFactory;
import gwendal.utils.AbstractAction;

public class FidelitiesAction extends AbstractAction {

	@Override
	public String executeAction(HttpServletRequest request) {
		// I retrieve a list of every fidelities from the dabatase
		List<Fidelity> fidelities = DAOFactory.getFidelityDAO().findAll();
		
		request.setAttribute("fidelities", fidelities);
		
		return "FidelitiesJSP";
	}

}
