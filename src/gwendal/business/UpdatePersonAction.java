package gwendal.business;

import javax.servlet.http.HttpServletRequest;

import gwendal.model.beans.Person;
import gwendal.model.dao.DAOFactory;
import gwendal.utils.AbstractAction;

public class UpdatePersonAction extends AbstractAction {

	@Override
	public String executeAction(HttpServletRequest request) {
		boolean postMethod = request.getMethod().equals("POST");
		
		Person person = null;
		String idStr = request.getParameter("id");
		
		try{
			try{
				// I parse the ID from GET and find the corresponding person
				int id = Integer.parseInt(idStr);
				person = DAOFactory.getPersonDAO().find(id);
			}catch(Exception e){ 
				e.printStackTrace();
			}
				// If I haven't submitted the form yet, I pass the person object to the view to fill the form
			if(!postMethod){
				request.setAttribute("person", person);
			}else{
				// I retrieve the data from the form
				String firstName = request.getParameter("firstName");
				String lastName = request.getParameter("lastName");
				String address = request.getParameter("address");
				String company = request.getParameter("company");
				String postCode = request.getParameter("postCode");
				String city = request.getParameter("city");
				String countryISO = request.getParameter("countryISO");
				String mail = request.getParameter("mail");

				// I hydrate the person object
				person.setFirstName(firstName);
				person.setLastName(lastName);
				person.setAddress(address);
				person.setCompany(company);
				person.setPostCode(postCode);
				person.setCity(city);
				person.setCountryISO(countryISO);
				person.setMail(mail);
				
				// I update it in the database
				DAOFactory.getPersonDAO().update(person);
				
				request.setAttribute("redirectUrl", "/person?id=" + person.getIdPerson());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return "PersonFormJSP";
	}
}
	
