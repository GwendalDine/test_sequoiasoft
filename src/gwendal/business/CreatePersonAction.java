package gwendal.business;

import javax.servlet.http.HttpServletRequest;

import gwendal.model.beans.Person;
import gwendal.model.dao.DAOFactory;
import gwendal.utils.AbstractAction;

public class CreatePersonAction extends AbstractAction{

	@Override
	public String executeAction(HttpServletRequest request) {
		
		if (request.getMethod().equals("POST")) {
			// I retrieve the form values
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String address = request.getParameter("address");
			String company = request.getParameter("company");
			String postCode = request.getParameter("postCode");
			String city = request.getParameter("city");
			String countryISO = request.getParameter("countryISO");
			String mail = request.getParameter("mail");
			
			try {
				// I create a new instance of Person and persist it in the database
				Person person = new Person(firstName, lastName, address, company, postCode, city, countryISO, mail);
				DAOFactory.getPersonDAO().create(person);
				request.setAttribute("redirectUrl", "/person?id=" + person.getIdPerson());
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return "PersonFormJSP";
	}

}