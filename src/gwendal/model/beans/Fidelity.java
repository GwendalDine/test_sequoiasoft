package gwendal.model.beans;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Fidelity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idPerson;
	
	private String code;
	private Date begin;
	private Date end;
	private int bonus;
	
	public Fidelity() {}
	public Fidelity(int idPerson, String code, Date begin, Date end, int bonus) {
		this.idPerson = idPerson;
		this.code = code;
		this.begin = begin;
		this.end = end;
		this.bonus = bonus;
	}
	
	public int getIdPerson() {
		return idPerson;
	}
	public void setIdPerson(int idPerson) {
		this.idPerson = idPerson;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public Date getBegin() {
		return begin;
	}
	public void setBegin(Date begin) {
		this.begin = begin;
	}
	
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	
	public int getBonus() {
		return bonus;
	}
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

}