package gwendal.model.dao;

import gwendal.model.beans.Fidelity;
import gwendal.model.beans.Person;

/**
 * Returns the different DAOs from the application
 */
public final class DAOFactory {
	private DAOFactory() {}
	
	public static InterfaceDAO<Person> getPersonDAO() {
		return new PersonDAO();
	}
	public static InterfaceDAO<Fidelity> getFidelityDAO() {
		return new FidelityDAO();
	}
	
}
