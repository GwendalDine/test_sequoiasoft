package gwendal.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import gwendal.model.beans.Fidelity;
import gwendal.utils.JPAUtil;

public class FidelityDAO implements InterfaceDAO<Fidelity> {
	private EntityManager em;
	
	public FidelityDAO() {
		em = JPAUtil.getEntityManager();
	}

	@Override
	public List<Fidelity> findAll() {
		TypedQuery<Fidelity> query = em.createQuery("from Fidelity", Fidelity.class);
		List<Fidelity> fidelities = query.getResultList();
		em.close();
		
		return fidelities;
	}

	@Override
	public Fidelity find(int id) {
		Fidelity fidelity = em.find(Fidelity.class, id);
		em.close();
		
		return fidelity;
	}

	@Override
	public void delete(int id) {
		EntityTransaction transaction = em.getTransaction();
		
		Fidelity fidelity = em.find(Fidelity.class, id);
		
		if(fidelity != null) {
			try {
				transaction.begin();
				em.remove(fidelity);	
				transaction.commit();
			}catch(Exception e) {
				transaction.rollback();
			}finally {
				if(em.isOpen()) {
					em.close();
				}
			}
		}
	}

	@Override
	public void update(Fidelity fidelity) { 
		EntityTransaction transaction = em.getTransaction();
		
		try {
			transaction.begin();
			em.persist(em.merge(fidelity));	
			transaction.commit();
		}catch(Exception e) {
			transaction.rollback();
		}finally {
			if(em.isOpen()) {
				em.close();
			}
		}
	}

	@Override
	public void create(Fidelity fidelity) { 
		EntityTransaction transaction = em.getTransaction();
		
		try {
			transaction.begin();
			em.persist(fidelity);	
			transaction.commit();
		}catch(Exception e) {
			transaction.rollback();
		}finally {
			if(em.isOpen()) {
				em.close();
			}
		}
	}
}
