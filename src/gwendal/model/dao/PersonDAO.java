package gwendal.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import gwendal.model.beans.Person;
import gwendal.utils.JPAUtil;

public class PersonDAO implements InterfaceDAO<Person>{
	private EntityManager em;
	
	public PersonDAO() {
		em = JPAUtil.getEntityManager();
	}

	@Override
	public List<Person> findAll() {
		TypedQuery<Person> query = em.createQuery("from Person", Person.class);
		List<Person> persons = query.getResultList();
		em.close();
		
		return persons;
	}

	@Override
	public Person find(int id) {
		Person person = em.find(Person.class, id);
		em.close();
		
		return person;
	}

	@Override
	public void delete(int id) {
		EntityTransaction transaction = em.getTransaction();
		Person person = em.find(Person.class, id);
		
		if(person != null) {
			try {
				transaction.begin();
				em.remove(person);	
				transaction.commit();
			}catch(Exception e) {
				transaction.rollback();
			}finally {
				if(em.isOpen()) {
					em.close();
				}
			}
		}
	}

	@Override
	public void update(Person person) { 
		EntityTransaction transaction = em.getTransaction();
		
		try {
			transaction.begin();
			em.persist(em.merge(person));	
			transaction.commit();
		}catch(Exception e) {
			transaction.rollback();
		}finally {
			if(em.isOpen()) {
				em.close();
			}
		}
	}

	@Override
	public void create(Person person) { 
		EntityTransaction transaction = em.getTransaction();
		
		try {
			transaction.begin();
			em.persist(person);	
			transaction.commit();
		}catch(Exception e) {
			transaction.rollback();
		}finally {
			if(em.isOpen()) {
				em.close();
			}
		}
	}
}
