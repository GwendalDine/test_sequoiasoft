<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Person</title>
	</head>
	<body>
		<p>ID : ${ person.idPerson }</p>
		<p>Lastname : ${ person.lastName }</p>
		<p>Address : ${ person.address }</p>
		<p>Postcode : ${ person.postCode }</p>
		<p>City : ${ person.city }</p>
	</body>
</html>