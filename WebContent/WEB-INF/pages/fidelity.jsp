<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Fidelity</title>
	</head>
	<body>
		<p>ID : ${ fidelity.idPerson }</p>
		<p>Code : ${ fidelity.code }</p>
		<p>Begin : ${ fidelity.begin }</p>
		<p>End : ${ fidelity.end }</p>
		<p>Bonus : ${ fidelity.bonus }</p>
	</body>
</html>