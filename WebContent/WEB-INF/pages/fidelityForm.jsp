<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Fidelity form</title>
	</head>
	<body>
		<form action="" method="POST">
			<input name="idPerson" type="number" value="${ fidelity.idPerson }">
			<input type="text" name="code" value="${ fidelity.code }" placeholder="Code">
			<input type="date" name="begin" value="${ fidelity.begin }" placeholder="Begin">
			<input type="date" name="end" value="${ fidelity.end }" placeholder="End">
			<input type="number" name="bonus" value="${ fidelity.bonus }" placeholder="Bonus">
			<input type="submit">
		</form>
	</body>
