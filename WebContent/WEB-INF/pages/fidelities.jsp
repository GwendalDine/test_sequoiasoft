<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<style>
		table, th, td{
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td{
			padding: 10px;
		}
	</style>
	<title>Fidelities</title>
	</head>
	<body>		
		<table>
			<tr>
				<td>ID</td>
				<td>Code</td>
				<td>Begin</td>
				<td>End</td>
				<td>Bonus</td>
			</tr>
			<c:forEach items="${ fidelities }" var="fidelity">
				<tr>
					<td>${ fidelity.idPerson }</td>
					<td>${ fidelity.code }</td>
					<td>${ fidelity.begin }</td>
					<td>${ fidelity.end }</td>
					<td>${ fidelity.bonus }</td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>