<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Home</title>
	<style>
		.warning{
			color: red;
		}
	</style>
	</head>
	<body>
		<p>/persons</p>
		<p>/person?id=</p>
		<p>/updateperson?id=</p>
		<p>/createperson</p>
		<p>/deleteperson?id=</p>
		<hr>
		<p>/fidelities</p>
		<p>/fidelity?id=</p>
		<p>/updatefidelity?id=</p>
		<p>/createfidelity</p>
		<p>/deletefidelity?id=</p>
	</body>
</html>