<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<style>
		table, th, td{
			border: 1px solid black;
			border-collapse: collapse;
		}
		th, td{
			padding: 10px;
		}
	</style>
	<title>Persons</title>
	</head>
	<body>
		<table>
			<tr>
				<th>ID</th>
				<th>Lastname</th>
				<th>Address</th>
				<th>Postcode</th>
				<th>City</th>
			</tr>
			<c:forEach items="${ persons }" var="person">
				<tr>
					<td>${ person.idPerson }</td>
					<td>${ person.lastName }</td>
					<td>${ person.address }</td>
					<td>${ person.postCode }</td>
					<td>${ person.city }</td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>