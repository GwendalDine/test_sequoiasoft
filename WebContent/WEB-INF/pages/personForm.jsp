<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Person form</title>
	</head>
	<body>
		<form action="" method="POST">
			<input name="idPerson" type="hidden" value="${ person.idPerson }">
			<input type="text" name="firstName" value="${ person.firstName }" placeholder="Firstname">
			<input type="text" name="lastName" value="${ person.lastName }" placeholder="Lastame">
			<input type="text" name="address" value="${ person.address }" placeholder="Address">
			<input type="text" name="company" value="${ person.company }" placeholder="Company">
			<input type="number" name="postCode" value="${ person.postCode }" placeholder="Postcode">
			<input type="text" name="city" value="${ person.city }" placeholder="City">
			<input type="text" name="countryISO" value="${ person.countryISO }" placeholder="Country ISO">
			<input type="email" name="mail" value="${ person.mail }" placeholder="Email">
			<input type="submit">
		</form>
	</body>
</html>